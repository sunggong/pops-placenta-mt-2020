# jpg
convert figures/fig3.heatmap.mito.higher.placenta.jpg figures/fig3.tsne.jpg -append figures/fig3.BC.jpg
convert figures/fig3.heatmap.mito2.jpg figures/fig3.BC.jpg +append figures/MAIN.FIG3.jpg

# pdf
convert figures/fig3.heatmap.mito.higher.placenta.pdf figures/fig3.tsne.pdf -append +repage figures/fig3.BC.pdf
convert figures/fig3.heatmap.mito2.pdf figures/fig3.BC.pdf +append +repage figures/MAIN.FIG3.pdf
