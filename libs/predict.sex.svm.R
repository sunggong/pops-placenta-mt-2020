source("libs/graphic.R")

###########################
## Train dataset          #
## placenta + endometrium #
###########################
dt.wtsi.doc.ratio<-merge(dt.wtsi.cov, dt.wtsi.cov[chr=="genome"], by=c("tissue","Barcode"))[,.(tissue,Barcode,chr=chr.x,DoC.ratio=DoC.x/DoC.y)]
dt.doc.dataset<-rbind(
    dt.doc.ratio[,.(`tissue`='placenta',Barcode,chr,DoC.ratio,Sex)][,`set`:=factor("train")],
    dt.wtsi.doc.ratio[tissue=="endometrium"][,`:=`(Sex="Female",set=factor('train'))],
    dt.wtsi.doc.ratio[tissue!="endometrium"][,`:=`(Sex='unknown',set=factor('test'))]
    )
dcast.data.table(dt.doc.dataset, set+tissue~Sex)
dt.doc.XY<-dcast.data.table(dt.doc.dataset[chr%in%c("X","Y")], set+tissue+Barcode+Sex~chr,value.var='DoC.ratio')
dt.doc.XY[,.N,.(set,tissue)]

p.doc.XY<-ggplot(dt.doc.XY, aes(X,Y)) +
        geom_point(aes(col=Sex,shape=tissue),alpha=.5,size=5) + 
        scale_shape_manual(values=c(8,9,0,1,2)) +
        ggsci::scale_color_aaas()+
        facet_wrap(~set) +
        geom_vline(xintercept=0.6,linetype='dashed') +
        geom_hline(yintercept=0.2,linetype='dashed') +
        #facet_grid(tissue~set) +
        theme_Publication()

##############################
## Manual Assignment of Sex ##
##############################
dt.doc.XY[X>0.6 & Y<0.2,pred.sex:='Female']
dt.doc.XY[X<0.6 & Y>0.2,pred.sex:='Male']
table(dt.doc.XY[,.(Sex,pred.sex,set)])

dt.MT.pred.sex<-merge(dt.doc.dataset[chr=="MT"],dt.doc.XY[,.(Barcode,pred.sex)])
dt.MT.pred.sex$pred.sex<-factor(dt.MT.pred.sex$pred.sex,levels=c("Male","Female","unknown"))
dt.MT.pred.sex[tissue%in%c("placenta","endometrium") & Sex!=pred.sex]
dt.MT.pred.sex[is.na(pred.sex),.N,tissue]
dt.MT.pred.sex[!is.na(pred.sex) ,.(.N,median(DoC.ratio)),.(tissue,pred.sex)][order(tissue,pred.sex)]

ggplot(dt.MT.pred.sex[!is.na(pred.sex) & !tissue%in%c("placenta","endometrium")], aes(pred.sex,DoC.ratio)) + 
    geom_boxplot(aes(fill=pred.sex),width=.5,size=.7,outlier.shape=NA,alpha=.6) + 
    geom_jitter(size=3, width=.1, alpha=.5) + 
    #scale_y_continuous(breaks=c(100,200,300,400)) +
    ylab("mtDNA copy number") +
    ggsci::scale_fill_aaas() + 
    ggsignif::geom_signif(comparisons=list(c("Male","Female"))) + 
    #facet_grid(~tissue,margins=T) + 
    facet_grid(~tissue,scales="free") + 
    theme_Publication() + theme(legend.position="")

###########################
## Sex Assignment via SVM #
###########################
library(e1071)
my.kernel='linear'
svmfit = e1071::svm(Sex ~ ., data = dt.doc.XY[set=="train",.(X,Y,Sex)], kernel = my.kernel, scale = TRUE)
plot(svmfit,dt.doc.XY[set=="train",.(Y,X,Sex)]) # check the X-Y axis

# find the best cost & gamma
tune.out <- e1071::tune(
                svm, Sex ~ ., data = dt.doc.XY[set=="train",.(X,Y,Sex)], kernel = my.kernel, scale=TRUE,
                ranges = list(cost = c(0.01, 0.1,1,10,100),
                               gamma = c(0.5,1,2,3,4))
)

# show best model
tune.out$best.model
tune.out$best.model$cost

# apply the best model 
svmfit = e1071::svm(Sex ~ ., data = dt.doc.XY[set=="train",.(X,Y,Sex)], kernel = my.kernel, cost=tune.out$best.model$cost, scale = TRUE)
plot(svmfit,dt.doc.XY[set=="train",.(X,Y,Sex)])

pred.sex<- predict(tune.out$best.model, dt.doc.XY[set=="train",.(X,Y,Sex)])
(misclass <- table(predict = pred.sex, truth = dt.doc.XY[set=="train"]$Sex))

# find the hyperplane
# it seems only work with 'svm(scale=FALSE)'
if(FALSE){
    foo<-as.matrix(dt.doc.XY[set=="train",.(X,Y)][svmfit$index]); colnames(foo)<-NULL
    head(foo)
    beta = drop(t(svmfit$coefs)%*%foo)
    #beta = drop(t(svmfit$coefs)%*%as.matrix(dt.doc.XY[set=="train",.(X,Y)][svmfit$index]))
    beta0 = svmfit$rho

    plot(dt.doc.XY[set=="train",.(X,Y)], col = dt.doc.XY[set=="train"]$Sex, pch = 19)
    points(dt.doc.XY[set=="train",.(X,Y)][svmfit$index], pch = 5, cex = 2)
    abline(beta0 / beta[2], -beta[1] / beta[2])
    abline((beta0 - 1) / beta[2], -beta[1] / beta[2], lty = 2)
    abline((beta0 + 1) / beta[2], -beta[1] / beta[2], lty = 2)
}

##
library(kernlab)
kernfit <- kernlab::ksvm(Sex~.,data=dt.doc.XY[set=="train",.(X,Y,Sex)],type = "C-svc", kernel = 'vanilladot') # , C = 100)
plot(kernfit, data = dt.doc.XY[set=='train',.(X,Y,Sex)])

###################
## Test dataset ##
###################
print(p.doc.XY)

# apply the best model 
pred.sex<- predict(tune.out$best.model, dt.doc.XY[,.(X,Y,Sex)])
table(pred.sex)
dt.doc.XY$pred.sex<-pred.sex
dt.doc.XY
table(dt.doc.XY[,.(Sex,pred.sex,set)])

dt.doc.pred.sex<-merge(dt.doc.dataset,dt.doc.XY[,.(Barcode,pred.sex)])
dt.doc.pred.sex$pred.sex<-factor(dt.doc.pred.sex$pred.sex,levels=c("Male","Female","unknown"))
p.MT.pred.sex<-
    ggplot(dt.doc.pred.sex[chr=="MT" & !tissue%in%c("endometrium")], aes(pred.sex,DoC.ratio)) + 
    geom_boxplot(aes(fill=pred.sex),width=.4,size=.7,outlier.shape=NA,alpha=.6) + 
    geom_jitter(size=3, width=.1, alpha=.3) + 
    ylab("mtDNA copy number") + xlab("Sex") +
    ggsci::scale_fill_aaas() + 
    ggsignif::geom_signif(comparisons=list(c("Male","Female"))) + 
    #facet_grid(~tissue,margins=T) + 
    facet_grid(~tissue,scales="free") + 
        scale_y_continuous(trans = log10_trans(),
                            breaks = trans_breaks("log10", function(x) 10^x),
                            labels = trans_format("log10", math_format(10^.x))) +
    theme_Publication() + theme(legend.position="")

# check X & Y again using pred.sex
    ggplot(dt.doc.pred.sex[chr%in%c("X","Y") & !tissue%in%c("placenta","endometrium")], aes(pred.sex, DoC.ratio)) + 
    geom_boxplot(aes(fill=pred.sex),width=.4,size=.2,outlier.shape=NA,alpha=.6) + 
    geom_jitter(size=3, width=.1, alpha=.3) + 
    ylab("Copy number")  +
    ggsci::scale_fill_aaas() + 
    ggsignif::geom_signif(comparisons=list(c("Male","Female")),na.rm=T) + 
    #facet_grid(~chr) + 
    facet_grid(tissue~chr) + 
    theme_Publication() 

dt.doc.pred.sex[tissue=="blood" & chr%in%c("MT","X","Y"),.(.N,median(DoC.ratio),sd(DoC.ratio)),.(chr,pred.sex,tissue)][order(chr)]
dt.doc.pred.sex[,.N,chr]
