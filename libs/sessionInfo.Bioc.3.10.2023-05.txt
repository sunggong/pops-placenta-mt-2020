R version 3.6.1 (2019-07-05)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Scientific Linux 7.9 (Nitrogen)

Matrix products: default
BLAS:   /usr/local/software/spack/spack-0.11.2/opt/spack/linux-rhel7-x86_64/gcc-5.4.0/r-3.6.1-zrytncqvsnw5h4dl6t6njefj7otl4bg4/rlib/R/lib/libRblas.so
LAPACK: /usr/local/software/spack/spack-0.11.2/opt/spack/linux-rhel7-x86_64/gcc-5.4.0/r-3.6.1-zrytncqvsnw5h4dl6t6njefj7otl4bg4/rlib/R/lib/libRlapack.so

locale:
 [1] LC_CTYPE=en_GB.UTF-8       LC_NUMERIC=C               LC_TIME=en_GB.UTF-8        LC_COLLATE=en_GB.UTF-8    
 [5] LC_MONETARY=en_GB.UTF-8    LC_MESSAGES=en_GB.UTF-8    LC_PAPER=en_GB.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C             LC_MEASUREMENT=en_GB.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
 [1] grid      parallel  stats4    stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] cowplot_1.0.0               ComplexHeatmap_2.2.0        ggvenn_0.1.3                dplyr_0.8.4                
 [5] magrittr_1.5                ggsci_2.9                   ggthemes_4.2.0              ggrepel_0.9.1              
 [9] R.utils_2.9.2               R.oo_1.23.0                 R.methodsS3_1.7.1           RColorBrewer_1.1-2         
[13] ggplot2_3.2.1               GOstats_2.52.0              graph_1.64.0                Category_2.52.1            
[17] Matrix_1.3-4                GO.db_3.10.0                AnnotationDbi_1.48.0        gprofiler2_0.1.9           
[21] edgeR_3.28.1                limma_3.42.2                DESeq2_1.26.0               SummarizedExperiment_1.16.1
[25] DelayedArray_0.12.2         BiocParallel_1.20.1         matrixStats_0.55.0          Biobase_2.44.0             
[29] rtracklayer_1.46.0          GenomicRanges_1.38.0        GenomeInfoDb_1.22.0         IRanges_2.20.2             
[33] S4Vectors_0.24.3            BiocGenerics_0.32.0         scales_1.1.0                nvimcom_0.9-106            
[37] data.table_1.12.8           colorout_1.2-2             

loaded via a namespace (and not attached):
 [1] colorspace_1.4-1         rjson_0.2.20             ellipsis_0.3.0           circlize_0.4.11         
 [5] htmlTable_1.13.3         XVector_0.26.0           GlobalOptions_0.1.2      base64enc_0.1-3         
 [9] clue_0.3-57              rstudioapi_0.10          bit64_0.9-7              splines_3.6.1           
[13] geneplotter_1.64.0       knitr_1.28               Formula_1.2-3            jsonlite_1.6.1          
[17] Rsamtools_2.2.3          annotate_1.64.0          dbplyr_1.4.2             cluster_2.1.2           
[21] png_0.1-7                compiler_3.6.1           httr_1.4.1               backports_1.1.5         
[25] assertthat_0.2.1         lazyeval_0.2.2           prettyunits_1.1.1        acepack_1.4.1           
[29] htmltools_0.4.0          tools_3.6.1              gtable_0.3.0             glue_1.3.1              
[33] GenomeInfoDbData_1.2.2   rappdirs_0.3.1           Rcpp_1.0.3               vctrs_0.3.2             
[37] Biostrings_2.54.0        xfun_0.12                stringr_1.4.0            lifecycle_0.2.0         
[41] XML_3.99-0.3             zlibbioc_1.32.0          hms_0.5.3                RBGL_1.62.1             
[45] curl_4.3                 memoise_1.1.0            gridExtra_2.3            biomaRt_2.42.0          
[49] rpart_4.1-15             latticeExtra_0.6-29      stringi_1.4.5            RSQLite_2.2.0           
[53] genefilter_1.68.0        checkmate_2.0.0          shape_1.4.4              rlang_0.4.7             
[57] pkgconfig_2.0.3          bitops_1.0-6             lattice_0.20-45          purrr_0.3.3             
[61] GenomicAlignments_1.22.1 htmlwidgets_1.5.1        bit_1.1-15.2             tidyselect_1.0.0        
[65] GSEABase_1.48.0          AnnotationForge_1.28.0   R6_2.4.1                 Hmisc_4.3-1             
[69] DBI_1.1.0                pillar_1.4.3             foreign_0.8-72           withr_2.1.2             
[73] survival_3.1-8           RCurl_1.98-1.1           nnet_7.3-16              tibble_3.0.3            
[77] crayon_1.3.4             BiocFileCache_1.10.2     plotly_4.9.1             progress_1.2.2          
[81] jpeg_0.1-8.1             GetoptLong_0.1.8         locfit_1.5-9.1           blob_1.2.1              
[85] Rgraphviz_2.30.0         digest_0.6.23            xtable_1.8-4             tidyr_1.0.2             
[89] openssl_1.4.1            munsell_0.5.0            viridisLite_0.3.0        askpass_1.1             
