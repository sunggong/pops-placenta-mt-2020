
##
##
if(TRUE){
    file.name<-file.path('figures/suppl/SI.fig.wgs.depth')
    jpeg(file=paste(file.name,'jpg', sep ='.'), width=10, height=6,units="in",res=150) 
    cowplot::plot_grid(p.doc.genome+xlab(""),p.doc.ratio.XY+xlab(""),labels="AUTO",label_size=25,rel_widths=c(1,2))
    dev.off()
}


######################################
## classifying depleted transcripts ##
######################################
if(FALSE){
    dl.pt.absent.3x[["Placenta.T3.pA"]][order(-FC)][1:5]

    # by nCPM
    (my.top.cpm<-dl.pt.absent.3x[["Placenta.T3.pA"]][order(My_mean)][1:5]$gene_id)
    (my.bottom.com<-dl.pt.absent.3x[["Placenta.T3.pA"]][order(-My_mean)][1:5]$gene_id)

    # by Effect size 
    (my.top.es<-dl.pt.absent.3x[["Placenta.T3.pA"]][order(My_mean-BG_min)][1:5]$gene_id)
    (my.bottom.es<-dl.pt.absent.3x[["Placenta.T3.pA"]][order(BG_min-My_mean)][1:5]$gene_id)

    # by FC
    (my.top.fc<-dl.pt.absent.3x[["Placenta.T3.pA"]][order(-FC)][1:5]$gene_id)
    (my.bottom.fc<-dl.pt.absent.3x[["Placenta.T3.pA"]][order(FC)][1:5]$gene_id)

    (my.tissues<-dt.meta[Source!="Singapore",.N,Tissue]$Tissue) # 46 GTEx + POPS
    li.norm.pc.tissue[["TMM"]][my.top.fc,][,my.tissues]

    dt.gtex.pt.cpm<-data.table(reshape2::melt(li.norm.pc.tissue[["TMM"]][dl.pt.absent.3x[["Placenta.T3.pA"]]$gene_id,my.tissues]))[order(Var1,value)]
    setnames(dt.gtex.pt.cpm,c("gene_id","Tissue","nCPM"))
    dt.gtex.pt.cpm[,.N,Tissue]
    dt.gtex.pt.cpm[,`:=`(Rank=seq_len(.N),COUNT=.N,Rank_PCT=(seq_len(.N)/.N)*100),gene_id]

    dt.gtex.pt.cpm %>% dcast.data.table(Rank~Tissue)

    merge(dt.gtex.pt.cpm[nCPM==dt.gtex.pt.cpm[, max(nCPM)]], dt.ensg[,.(ensembl_gene_id,hgnc_symbol)],by.x="gene_id", by.y="ensembl_gene_id")
    merge(dt.gtex.pt.cpm[nCPM==dt.gtex.pt.cpm[, min(nCPM)]], dt.ensg[,.(ensembl_gene_id,hgnc_symbol)],by.x="gene_id", by.y="ensembl_gene_id")

    (p1<-ggplot(dt.gtex.pt.cpm, aes(Rank,nCPM,group=gene_id)) +
        geom_point(size=.5,alpha=.5,col="grey10") +
        geom_line(alpha=.3,col="grey70") +
        #ggrepel::geom_text_repel(data=dt.gtex.pt.cpm[Tissue=="Placenta.T3.pA" & nCPM==0],label="Placenta",direction="y",hjust=0,nudge_x=.7,nudge_y=20000,col=cbPalette2[1],size=5) +
        theme_Publication()
    )

    (p2<-ggplot(dt.gtex.pt.cpm, aes(Rank,nCPM,group=gene_id)) +
        geom_point(size=.5,alpha=.5,col="grey10") +
        geom_line(alpha=.3,col="grey70") +
        geom_line(data=dt.gtex.pt.cpm[gene_id %in% my.top.fc],alpha=.7,col="red3") +
        geom_line(data=dt.gtex.pt.cpm[gene_id %in% my.bottom.fc],alpha=.7,col="blue") +
        coord_cartesian(xlim=c(1:3), ylim=c(0:(dt.gtex.pt.cpm[Rank<=3, max(nCPM)]+100))) + 
        theme_Publication()
    )

    myCol  <- c(
                "Top5 (Effect size)" = "orange",
                "Bottom5 (FC)" = "blue",
                "Bottom5 (Effect size)" = "green4", 
                "Top5 (FC)" = "red3")

    (p3<-ggplot(dt.gtex.pt.cpm, aes(Rank,nCPM+0.001,group=gene_id)) +
        geom_point(size=.5,alpha=.5,col="grey10") +
        geom_line(alpha=.3,col="grey70") +
        geom_line(data=dt.gtex.pt.cpm[gene_id %in% my.top.es],alpha=.95,aes(color="Top5 (Effect size)")) +
        geom_line(data=dt.gtex.pt.cpm[gene_id %in% my.bottom.fc],alpha=.95,aes(color="Bottom5 (FC)")) +
        geom_line(data=dt.gtex.pt.cpm[gene_id %in% my.bottom.es],alpha=.95,aes(color="Bottom5 (Effect size)")) +
        geom_line(data=dt.gtex.pt.cpm[gene_id %in% my.top.fc],alpha=.95,aes(color="Top5 (FC)")) +
        scale_y_log10(breaks = trans_breaks("log10", function(x) 10^x),
                    labels = trans_format("log10", math_format(10^.x))) +
        labs(y="log10(nCPM)", color="Legend") +
        scale_color_manual(values = myCol) +
        theme_Publication()
    )

    file.name<-file.path('figures/suppl/SI.fig.762.depleted.tr.rank.all.tissues')
    pdf(file=paste(file.name,'pdf', sep ='.'), width=17, height=14,title="SI Figure for 762 transcripts") 
    print(p3)
    dev.off()
}

################
## GO analysis #
################
if(FALSE){
    my.target.ensg.5x<-dl.pt.absent[["Placenta.T3.pA"]][BG_min/My_mean>5][,.N,gene_id][N==2]$gene_id; length(my.target.ensg.5x)
    my.target.ensg.10x<-dl.pt.absent[["Placenta.T3.pA"]][BG_min/My_mean>10][,.N,gene_id][N==2]$gene_id; length(my.target.ensg.10x)

    (p.5x<-plot_goprofiler(my.target.ensg.5x,my.bg.ensg,max.depth=100) )
    (p.10x<-plot_goprofiler(my.target.ensg.10x,my.bg.ensg,max.depth=100) )

    file.name<-file.path('figures/suppl/SI.fig.GO')
    jpeg(file=paste(file.name,'jpg', sep ='.'), width=11, height=16,units="in",res=150) 
    # merge-all
    cowplot::plot_grid(p.common+theme(axis.text = element_text(size=rel(.9)),legend.position="top"),
                       p.5x+theme(axis.text = element_text(size=rel(1.2)),legend.position=""),
                       #p.10x,
                       nrow=2,labels="AUTO",label_size=25,align="v",axis="t",
                       #rel_heights=c(2.5,1.7,1)
                       rel_heights=c(3,1)
    )
    dev.off()
}

