tissue.ab,tissue
Biliary,Biliary
Bladder,Bladder
Bone,Bone/soft tissue
Breast,Breast
CNS,CNS
Cervix,Cervix
ColoRect,Colon/Rectum
Eso,Esophagus
Head,Head/Neck
Kidney,Kidney
Liver,Liver
Lung,Lung
Lymph,Lymphoid
Myeloid,Myeloid
Ovary,Ovary
Panc,Pancreas
Prost,Prostate
Skin,Skin
SoftTissue,Bone/soft tissue
Stomach,Stomach
Thy,Thyroid
Uterus,Uterus
