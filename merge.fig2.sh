convert figures/fig2.heatmap.T123.jpg figures/fig2.heatmap.GO.KEGG.7x10.jpg +append figures/MAIN.FIG2.jpg
convert figures/fig2.heatmap.T123.pdf figures/fig2.heatmap.GO.KEGG.7x10.pdf +append +repage figures/MAIN.FIG2.pdf

# alternatively use this for pdf horizontal pdf merge
#pdfjam figures/fig2.heatmap.T123.pdf figures/fig2.heatmap.GO.KEGG.7x10.pdf --nup 2x1  --landscape --outfile figures/MAIN.FIG2.pdf
