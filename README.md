[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7938169.svg)](https://doi.org/10.5281/zenodo.7938169)

## Introduction
This is a collection of custom scripts, mainly to generate figures, used for the following paper: "The human placenta exhibits a unique transcriptomic void", Gong *et al.* 2023. *Cell reports*, 2023". 

## How to cite this material
doi:10.5281/zenodo.7938169

## Abstract
> We have recently demonstrated that the human placenta exhibits a unique genomic architecture with an unexpectedly high mutation burden1 and it is also well recognized that the placenta uniquely expresses many genes2. However, the placenta is relatively understudied in systematic comparisons of gene expression in different organs. The aim of the present study was to identify transcripts which were uniquely absent or depleted, comparing the placenta with 46 other human organs. Here we show that 40/46 of the other organs had no transcripts which were selectively depleted and that of the remaining six, the liver had the largest number with 26. In contrast, the term placenta had 762 depleted transcripts. Gene Ontology analysis of this depleted set highlighted multiple pathways reflecting known unique elements of placental physiology. For example, transcripts associated with neuronal function are in the depleted set and this is expected given the lack of placental innervation. However, analysis of the depleted transcripts in term samples demonstrated massive over representation of genes involved in mitochondrial function (P=5.8x10-10), including PGC-1α - the master regulator of mitochondrial biogenesis, and genes involved in polyamine metabolism (P=2.1x10-4). We conclude that the term placenta exhibits a unique metabolic environment.

## The R scripts

### Built-in functions and parameters
+ [`libs/_local_meta.R`](libs/_local_meta.R): The configuration file, e.g. the version and the file locations:
```r
my.gtex.ver="v8.p2" # v8.p2 (gencode v26; Ensembl v88)

###############
## Meta info ##
###############
message("loading meta info...")
my.pt3.pa.meta<-"data/Meta/placenta.T3.pA.meta.csv"
my.pt1.pa.meta<-"data/Meta/placenta.T1.pA.meta.csv"
my.gtex.meta<-file.path("data/Meta",paste0("GTEx.",my.gtex.ver,".meta.ALL.csv"))
```

+ [`libs/local.R`](libs/local.R): defines built-in functions used in other R files such as [`MAIN.FIGURES.R`](MAIN.FIGURES.R) which generate the main figures in the paper. Most of "heavy-lifting" modules, such as  `DESeq2` and `edgeR`, are within the file, e.g.:
```r
##############
# run DESeq2 #
##############
runDESeq<-function(mat.cnt, df.meta){
    register(MulticoreParam(32))
    message("DESeqDataSetFromMatrix...")
    #deseq.design=formula(~Sex)
    #dds <- DESeqDataSetFromMatrix(mat.cnt, df.meta, deseq.design)
    dds <- DESeqDataSetFromMatrix(mat.cnt, df.meta, formula(~1))
    message("estimateSizeFactors...")
    dds <- estimateSizeFactors(dds)
    rowRanges(dds) <- gr.target.list[rownames(dds)] # sorted by the original rownames
    return(dds)
}
```

+ [`libs/preprocess.R`](libs/preprocess.R): it preprocesses and loads the cached results (i.e. *RData) that are used in other R files, e.g.:
```r
init_norm_tissue(protein.coding=T) # load 'li.norm.pc.tissue'

####################
## Find the bottom #
###################
myRData=file.path('data/dl.bottom.absent.RData')
if(file.exists(myRData)){
    load(myRData) # load 'dl.bottom' & 'dl.absent'
}else{
    dl.bottom<-lapply(my.datasets, function(my)
                    lapply(li.norm.pc.tissue[c("DESeq","TMM")], function(i) find_bottom(i,my))
    )
    names(dl.bottom)<-my.datasets
    lapply(dl.bottom, function(i) lapply(i,nrow))

    # at least 2x
    dl.absent<-lapply(my.datasets, function(my)
                        merge(rbind(
                        dl.bottom[[my]][["DESeq"]][BG_min>1 & BG_min/My_mean>2][,method:='DESeq'],
                        dl.bottom[[my]][["TMM"]][BG_min>1 & BG_min/My_mean>2][,method:='TMM']
                        ),
                        dt.ensg[,.(ensembl_gene_id,hgnc_symbol,chromosome_name)],
                        by.x="gene_id",by.y="ensembl_gene_id",all.x=T)
    )
    names(dl.absent)<-my.datasets
    lapply(dl.absent, function(i) i[,.N,method])
    save(dl.bottom,dl.absent,file=myRData)
}
dl.pt.bottom<-dl.bottom[my.pt.datasets]
```

### The main & supplemental figures
The main figures were generated from [`MAIN.FIGURES.R`](MAIN.FIGURES.R) and the supplementary figures were generated from [`SUPPL.FIGURES.R`](SUPPL.FIGURES.R).

### The supplementary tables
Have a look at [`SUPPL.TABLES.R`](SUPPL.TABLES.R) and [`SUPPL.INFO.R`](SUPPL.FIGURE.1.R) files.

## Figures
### Figure 1
![Figure 1](figures/MAIN.FIG1.jpg)
### Figure 2
![Figure 2](figures/MAIN.FIG2.jpg)
### Figure 3
![Figure 3](figures/MAIN.FIG3.jpg)
### Figure 4
![Figure 4](figures/MAIN.FIG4.jpg)

## SessionInfo()
Everytime when you load the [`libs/local.R`](libs/local.R) file, it will generate a R session file in 'libs' folder, e.g.:
```r
writeLines(capture.output(sessionInfo()),file.path('libs',paste("sessionInfo","Bioc",BiocManager::version(),time.stamp,"txt",sep=".")))
```

See the latest file from [here](libs/sessionInfo.Bioc.3.10.2023-05.txt)
