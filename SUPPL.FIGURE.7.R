
    my.measure<-'TMM'
    #################################
    ## based on 'li.norm.pc.tissue' #
    #################################
    ## 26 GTEx tissues + 3 placentas (8wk,14wk,term)
    # by chromosome_name
    plot_read_proportion(my.measure,my.type="chromosome_name",return.table=T)
    (p1.map<-plot_read_proportion(my.measure,my.type="chromosome_name",protein.coding=T) + 
        xlab("") + ylab("Percentage      ") +
        theme(legend.position=c(.8,.58),
              legend.text=element_text(size=rel(.8)),
              legend.title = element_text(face="bold.italic",size=rel(.8)),
              axis.title = element_text(size = rel(.9)),
              plot.margin=unit(c(.5,.2,0,.5),"cm"),
              axis.text.x = element_text(angle=40,hjust=1,size=rel(.7)), 
              axis.text.y=element_text(size=rel(.7))
        )
    )

    # GTEx + this study + external placenta
    (my.ext.pt<-dt.ext.pt[!grepl("Majewska",Tissue) & !grepl("Buckberry",Tissue)]$SubTissue)
    names(my.ext.pt)<-dt.ext.pt[!grepl("Majewska",Tissue) & !grepl("Buckberry",Tissue)]$Tissue
    my.whitelist<-c(whitelist.short,my.ext.pt)

    # 20 GTEx tissues + 3 placentas (8wk,14wk,term)
    # but, based on 'li.norm.pc.tissue' 
    not.this<-whitelist.short %in% c("Ovary","Pituitary","Prostate","Testis","Uterus","Vagina")
    whitelist.short[!not.this] %>% length

    plot_read_proportion(my.measure,my.type="chromosome_name",protein.coding=T,return.table=T,whitelist.short[!not.this])
    (p1.map<-plot_read_proportion(my.measure,my.type="chromosome_name",protein.coding=T,my.whitelist=whitelist.short[!not.this]) + theme(axis.text.x=element_text(angle=-45,hjust=0,size=rel(.8))))

    ###############################
    ## based on 'li.norm.tissue' ##
    ###############################
    # 20 GTEx tissues + 3 placentas (8wk,14wk,term)
    # by chromosome_name
    plot_read_proportion(my.measure,my.type="chromosome_name",protein.coding=F,return.table=T,my.whitelist=whitelist.short.23)
    p1.map1<-plot_read_proportion(my.measure,my.type="chromosome_name",protein.coding=F,return.table=F,my.whitelist=whitelist.short.23) + theme(axis.text.x=element_text(angle=-45,hjust=0,size=rel(.8)))

    # by gene_biotype
    plot_read_proportion(my.measure,my.type="gene_biotype",protein.coding=F,return.table=T,my.whitelist=whitelist.short.23)
    p1.map2<-plot_read_proportion(my.measure,my.type="gene_biotype",protein.coding=F,return.table=F,my.whitelist=whitelist.short.23) + theme(axis.text.x=element_text(angle=-45,hjust=0,size=rel(.8)))

    file.name<-file.path('figures/suppl/SI.FIG7')
    jpeg(file=paste(file.name,'jpg', sep ='.'), width=9, height=8,units="in",res=300)
    cowplot::plot_grid(p1.map, p1.map1, nrow=2,labels="AUTO",label_size=25, align="v")
    dev.off()
