
# POPS placenta methylation datasets (GRCh37)
load("data/dt.meth.PT.RData") # dt.meth.PT
dt.meth.PT[,.N,Region]

# genes of interests
dt.depleted<-fread("tables/depleted.in.Placenta.T3.pA.csv")
dt.enriched<-fread("tables/placenta.enriched.protein_coding.rZ.csv")
dt.target<-rbind(
    dt.depleted[,.(gene_id,`symbol`=hgnc_symbol,`category`="depleted",Exp=Placenta.T3.pA)], # n=762 depleted in T3
    dt.enriched[,.(`gene_id`=ensembl_gene_id,`symbol`=hgnc_symbol,`category`="enriched",Exp=Placenta)] # n=71 enriched in T3
    )
dt.target[,.(.N,Mean=mean(Exp),Median=median(Exp)),category]

# regional information (GRCh37) from RnBeads.hg19 (https://bioconductor.org/packages/3.2/data/experiment/html/RnBeads.hg19.html)
load("data/regions.RData")
regions %>% names
dt.gene<-as.data.table(as.data.frame(regions[['promoters']])) # GRCh37
dt.promo<-as.data.table(as.data.frame(regions[['promoters']])) # GRCh37
dt.cpgi<-as.data.table(as.data.frame(regions[['cpgislands']])) # GRCh37

######################################
# promoters in the target gene list ##
######################################
dt.target.promo<-merge(dt.promo[,.(chr=substr(seqnames,4,length(seqnames)),start,end,strand,symbol)], dt.target)
dt.target.promo[,.N,category]
dt.target.promo %>% dcast.data.table(chr~category,length)

#############################################
# CPGi wihtin 10kb of the target gene list ##
#############################################
dt.gene[,.(seqnames,start,end,strand,symbol)]
(dt.target.gene<-merge(dt.gene[,.(seqnames,start,end,strand,symbol)], dt.target))
dt.target.gene[,.N,category]
dt.target.gene.10kb=dt.target.gene[,.(seqnames,start=start-1e+4,end=end+1e+4,strand,gene_id,symbol,category)]
dt.target.gene.10kb[,.N,category]

setkey(dt.target.gene.10kb,seqnames,start,end)
dt.target.cpgi<-foverlaps(
          dt.cpgi,
          dt.target.gene.10kb
          ,nomatch=NULL
)[,.(chr=substr(seqnames,4,length(seqnames)),start=i.start,end=i.end,gene_id,symbol,category)][order(chr,start,end)]
dt.target.cpgi[,.(.N,length(unique(gene_id))),category]

####################################################
## Methylation (gene-level) at the promoter region #
####################################################
setkey(dt.target.promo,chr,start,end)
system.time(dt.pt.meth.promo<-foverlaps(
                          dt.meth.PT[Region=="Promo_15.05"],
                          dt.target.promo,nomatch=NULL
)[,.(chr,start,end,gene_id,symbol,category,strand,i.start,i.end,meth,Gender,Tissue,Region="Promoter")])

dt.pt.meth.promo[,.(.N,`N_gene`=length(unique(symbol)),`Mean`=mean(meth*100),`Median`=median(meth*100)),.(category,Region)]
dt.pt.meth.promo[,.(.N,`Mean`=mean(meth*100),`Median`=median(meth*100)),.(category,Region,symbol)]


##########################################################
# CpG methylation (base-level) within the target region ##
##########################################################
dl.meth.target<-list()
# CpGi
setkey(dt.target.cpgi,chr,start,end)
system.time(dl.meth.target[["CpG island"]]<-foverlaps(
                          dt.meth.PT[Region=="Sites"],
                          dt.target.cpgi,nomatch=NULL
)[,.(chr,start,end,gene_id,symbol,category,strand,i.start,i.end,meth,Gender,Tissue,Region="CpG island")])


# Prmoter
setkey(dt.target.promo,chr,start,end)
system.time(dl.meth.target[["Promoter"]]<-foverlaps(
                          dt.meth.PT[Region=="Sites"],
                          dt.target.promo,nomatch=NULL
)[,.(chr,start,end,gene_id,symbol,category,strand,i.start,i.end,meth,Gender,Tissue,Region="Promoter")])

#
dt.meth.target<-rbindlist(dl.meth.target)
dt.meth.target[symbol =="CHL1"][,.(`N_cpg`=.N,`N_gene`=length(unique(gene_id)),`Mean`=mean(meth),`Median`=median(meth)),.(category,Tissue,Region)]
dt.meth.target[symbol =="MRPL41"][,.(`N_cpg`=.N,`N_gene`=length(unique(gene_id)),`Mean`=mean(meth),`Median`=median(meth)),.(category,Tissue,Region)]

(dt.meth.target[Region=="CpG island",.(.N,mean(meth)),.(Tissue,symbol)] %>% dcast.data.table(Tissue~symbol))[,1:6]
dt.meth.target[Tissue=="PT",.(`N_cpg`=.N,`N_gene`=length(unique(symbol)),`Mean`=mean(meth),`Median`=median(meth)),.(category,Tissue,Region)]

wilcox.test(dt.meth.target[Tissue=="PT" & Region=="CpG island" & category=="depleted"]$meth,
            dt.meth.target[Tissue=="PT" & Region=="CpG island" & category=="enriched"]$meth
            )

wilcox.test(dt.meth.target[Tissue=="PT" & Region=="Promoter" & category=="depleted"]$meth,
            dt.meth.target[Tissue=="PT" & Region=="Promoter" & category=="enriched"]$meth
            )

#
dt.depleted[My_mean<1][order(-BG_min+My_mean)][1:10]
dt.enriched[order(-Placenta)][1:20]

# per type
dt.meth.target[Tissue=="PT",.(`N_cpg`=.N,`N_gene`=length(unique(symbol)),`Mean`=mean(meth),`Median`=median(meth)),.(category,Tissue,Region)]
# per type per gene
(dt.meth.target.gene<-dt.meth.target[,.(`N_cpg`=.N,`N_gene`=length(unique(symbol)),`Mean`=mean(meth),`Median`=median(meth)),.(category,Tissue,Region,symbol)])
dt.meth.target.gene[Tissue=="PT",.(Total_CpG=sum(N_cpg),.N,`Mean`=mean(Mean),`Median`=median(Mean)),.(category,Tissue,Region)]
dt.meth.target.gene[Tissue=="PT" & N_cpg>=10,.(Total_CpG=sum(N_cpg),.N,`Mean`=mean(Mean),`Median`=median(Mean)),.(category,Tissue,Region)]

# save the meth level of PT
fwrite(dt.meth.target.gene, file="tables/dt.methtarget.gene.PT.csv.gz")

dt.depleted[My_mean<1][order(-BG_min+My_mean)][1:20]
dt.enriched[order(-Placenta)][1:20]

# for some genes only
my.gene.id<-c(
              dt.depleted[My_mean<1][order(-BG_min+My_mean)][1:20]$gene_id, # top10 most depleted gene
              dt.enriched[order(-Placenta)][1:20]$ensembl_gene_id # top10 most enriched
              )
dt.meth.target[Tissue=="PT" & gene_id %in% my.gene.id,.(`N_cpg`=.N,`N_gene`=length(unique(symbol)),`Mean`=mean(meth),`Median`=median(meth)),.(category,Tissue,Region)]


p.site.beta<-ggplot(dt.meth.target[Tissue=="PT"], aes(meth*100)) +
			geom_line(stat="density", size=1) +
			labs(x="Methylation level (%)",y="Density") +
			#scale_colour_manual(name="",values=my.col[["Gender"]]) +
            facet_grid(Region~category) +
			theme_Publication() +
			theme(legend.position="none")

my_iqr<-function(x){
    foo<-unname(quantile(x,na.rm=T))
    m <-foo[3] 
    ymin <-foo[2] 
    ymax <-foo[4] 
    return(c(y=m,ymin=ymin,ymax=ymax))
}

p.site.meth.iqr<-
    ggplot(dt.meth.target[Tissue=="PT"], aes(category,meth*100)) +
    #geom_boxplot(na.rm=T,width=.5,size=.8,outlier.shape="",alpha=.7) +
    stat_summary(fun.data=my_iqr, size=1.1) + # my_iqr defined within local.R
    ggsignif::geom_signif(comparisons=list(c("enriched","depleted")),size=.3,textsize=6,tip_length=0.02,y_position=50) + 
	labs(y="Methylation level (%)",x="Type of transcript") +
    facet_wrap(~Region) +
    theme_Publication() 

p.gene.meth<-ggplot(dt.meth.target.gene[Tissue=="PT" & N_cpg>=10], aes(category,Mean*100)) +
    geom_boxplot(na.rm=T,width=.3,size=1.5,outlier.shape="",alpha=.5) +
    geom_jitter(width=.1,size=1.7,alpha=.4) +
    ggsignif::geom_signif(comparisons=list(c("enriched","depleted")),size=.3,textsize=6,tip_length=0.02) + 
	labs(y="Methylation level (%)",x="Type of transcript") +
    facet_wrap(~Region) +
    theme_Publication() 


file.name<-file.path('figures/suppl/SI.FIG11')
pdf(file=paste(file.name,'pdf', sep ='.'), width=14, height=9,title="Methylation level") 
#p.site.beta
#p.site.meth.iqr
p.gene.meth
dev.off()

